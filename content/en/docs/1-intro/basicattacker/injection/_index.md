---
title: "Injection"
date: 2020-08-21
weight: 30
---
{{< pg_start >}}


### Message creation

Eve can also create messages, as if she were a normal agent in the system (which she might), and possibly spoofing the identity of the sender.

For instance, Eve can send the message "fake" (assuming Eve knows that message) to Bob pretending to be Alice. 

<div class='game'>
    <div class='agents' id='agents_box'></div>
    <div class='network' id='bar'>
        <center>Network stack</center>
        <ul id='message_list'></ul>
    </div>
</div>

<div class='steps'>
  <button onclick="step40();">Step 0</button>
  <button onclick="step41();">Step 1 (E sends message)</button>
  <button onclick="step42();">Step 2 (message transmitted)</button>
</div>

<script>

    class Listener extends Agent {
        process (msg, step) {
    	    var facts = msg.content.getFacts();
    	    for (var i = 0; i < facts.length; i++) {
    	        this.learns(facts[i], step);
    	    }
        }
    }
    
    let s = new NetStack('message_list');
    let a = new Agent('A', ['secret'], s);
    let b = new Listener('B', [], s);
    let e = new Agent('E', ['fake'], s);
    s.registerAgents([a, b, e]);
    s.registerAttacker(e);
    
    a.createDiv('agents_box', 'agent');      
    b.createDiv('agents_box', 'agent');
    e.createDiv('agents_box', 'adversary');
    
    
    setCurrentStep(0);
    
    function step40 () {
        clearBox(0, 'box');
        b.forgets(0);
        b.updateContent();
        s.clearStack(0);
        setCurrentStep(0);
    }
    
    function step41 () {
        step40();
        clearBox(1, 'box');
        s.injectMessage(e, create_message('A -> B:fake'), 1, 'w');
        s.refreshStack();
        setCurrentStep(1);	
    
    }
    
    function step42 () {
        step41();
        clearBox(2, 'box');
        s.transmitMessage(0, 2);
        b.updateContent();
        s.refreshStack();
        setCurrentStep(2);	 
    }
    
</script>

The assumption that Eve knows the message "fake" is very important (which is indicated above by using the color black for "fake" in Eve), as we do not want to consider the case where an adversary can send messages containing information they do not have access to. For instance, in the case above, Eve cannot send the message "secret", because she does not know it. 

  {{< protocolgame_svg >}}
