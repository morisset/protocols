 ---
title: "Identity Recipient"
date: 2020-02-16
weight: 40
---
{{< pg_start >}}


The problem with the previous protocol is that when A receives the message
{K_0, {K_0}KES}KAS from the server, it does not know that K_0 is encrypted with KES, and not KBS. This attack is known as a **man-in-the-middle attack**, where E intercepts and modify communication between A and S. 

To avoid this attack, the protocol below introduces the **explicit identity** of the intended recipient in the message back from the server. 

<b>Knowledge:</b> A:[s, KAS, A, B], B:[KBS, A, B],
  S:[A, B, E, KAS, KBS, KES, K] E:[A, B, KES, fake]<br>
<b>Steps:</b>
<ol>
   <li> A -> S: A, B
   <li> S -> A: {#K, B, {K}KBS}KAS
   <li> A -> B: {K}KBS, {#s}K
</ol>

If E tried the same attack as before, the server would return {K_0, E, {K}KES}KAS, and A would detect the attack.

However, another type of attack is possible. We now assume that it's possible for an attack to compromise a past session key with the following command: 

* <span class='cmd'>compromise(key)</span>: the attacker will know the key <span class='cmd'>key</span>, only if that key was used in a previous session by the main agent A. 

In other words, if A starts a new session with a session secret s_1, and receives the key K_0, once A starts a new session (i.e., with a new secret s_2), then the attacker can compromise K_0. This assumption corresponds to the fact that session keys can be eventually leaked or brute-forced. 
	  
	  
{{< protocolgame >}}

<script>	  
 class Sender extends Agent {
	      
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.session = 0;
		  this.state = 0;
		  this.currentKey = null;
	      }
	      
	      // Sender has the following states
	      // init: initial state, ready to send message 1
	      // (in case of multiple sessions, message 3 from previous session
	      // has been sent)
	      // waiting_2: message 1 sent, waiting for message 2. 
	      
	      process (msg, step) {
		  switch (this.state) {
		  case 0:
		      this.init(step); break;
		  case 1:
		      this.sendEncryptedSecret(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		  }
	      }
	      
	      getSessionSecret (step) {
		  var secret = 's_' + this.session;
		  this.learns(secret, step)
		  return secret;
	      }

	      
	      sendEncryptedSecret(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 1) { 
		      console.log('Expecting exactly one argument, received: ' + facts.length);
		      return;
		  }
		  
		  var KBS_KAS = facts[0];
		  this.learns(KBS_KAS, step);

		  console.log('KBS_KAS: ' + KBS_KAS) ;

		  var facts2 = this.decrypt(KBS_KAS, 'KAS', step).getFacts();
		  if (facts2.length != 3) { 
		      console.log('Expecting exactly three inner messages, received: ' + facts.length);
		      return;
		  }
		      
		  var K = facts2[0];
		  this.currentKey = K;
		  var B = facts2[1];
		  if (B != 'B') {
		      throw 'Wrong sender received: ' + B + ' (expecting B)'; 
		  }

		  var K_KBS = facts2[2];

		  var S_K = this.encrypt(this.getSessionSecret(step), K);
		  var new_msg = {source:this.id, destination:'B',
				 content:K_KBS + ', ' +S_K};
		  this.sendMessage(new_msg, step);
		  this.state = 0;
	      }
	      
	      init (step) {
		  this.session++;
		  this.getSessionSecret(step);
		  this.network.pastKeys.push(this.currentKey);
		  var msg = {source:this.id, destination:'S', content:'A, B'}; 
		  this.sendMessage(msg, step);
		  this.state = 1;
	      }
	  }

	  class Receiver extends Agent {
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.state = 0;
	      }

	      process(msg, step) {
		  switch (this.state) {
		  case 0:
		      this.decryptMessage(msg, step);
		      break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		      
		  }


	      }

	      decryptMessage(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 2) { 
		      throw (this.id + ': Expecting exactly two arguments, received: ' + facts.length);
		      return;
		  }

		  var K_KBS = facts[0];
		  var S_K = facts[1];
		  this.learns(K_KBS);
		  this.learns(S_K);

		  var K = this.decrypt(K_KBS, 'KBS', step);
		  var S = this.decrypt(S_K, K, step);
		  this.step = 0;
		  
	      }

	  }

	  class Server extends Agent {
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.state = 0;
		  this.key = null;
		  this.session = 0;
	      }

	      
	      getFreshKey(step) {
		  var k = 'K_' + this.session++;
		  this.learns(k, step)
		  this.key = k;
		  return k;
	      }

	      process (msg, step) {
		  switch (this.state) {
		  case 0:
		      this.sendEncryptedKey(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		  }
	      }
	      
	      sendEncryptedKey(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 2) { 
		      console.log('Expecting exactly two arguments, received: ' + facts.length);
		      return;
		  }
		  
		  var A = facts[0];
		  var B = facts[1];
		  var K = this.getFreshKey(step);
		  this.learns(K, step);
		  this.learns(B);
		  
		  var KAS = 'K' + A + 'S';
		  var KBS = 'K' + B + 'S';

		  var K_KBS = this.encrypt(K, KBS, step);
		  
		  var new_msg = {source:this.id, destination:A,
				 content:this.encryptList([K, B, K_KBS], KAS, step)};
		  this.sendMessage(new_msg, step);
		  
	      }

	  }
	  
	  class Attacker extends Agent {
	      process (msg) {
		  var facts = msg.content.getFacts();
		  for (var i = 0; i < facts.length; i++)
		      this.learns(facts[i]);
	      }

	      compromise(key) {
		  if (this.network.pastKeys.includes(key)) {
		      this.learns(key);
		  }
		  else
		      throw key + ' is not a past key, cannot be compromised';
	      }
	  }
	  
	  var s1 = new NetStack('message_list1');	  
	  var a1 = new Sender('A', ['s', 'KAS', 'A', 'B'], s1);
	  var b1 = new Receiver('B', ['A', 'B', 'KBS'], s1);
	  var v1 = new Server('S', ['KES', 'KAS', 'KBS', 'A', 'B', 'E'], s1);
	  var e1 = new Attacker('E', ['E', 'A', 'B', 'fake', 'KES'], s1);
	  s1.registerAgents([a1, b1, v1, e1]);
	  s1.registerAttacker(e1);
	  var c1 = new Command(s1, e1, [a1, b1, e1, v1]);
	  
	  a1.createDiv('agents_box1', 'agent');      
	  b1.createDiv('agents_box1', 'agent');
	  v1.createDiv('agents_box1', 'agent');
	  e1.createDiv('agents_box1', 'agent');

	  setCurrentStep(0);
	  
</script>

## Attacks


1. Can you find a sequence of commands such that E knows s_2? 
* {{< tip "Tip 1" tip1 >}}
The idea of the attack is to compromise the first session key, so try to fully run a session, then start a new session, and then compromise the initial session key. 
{{< /tip >}}

* {{< tip "Tip 2" tip2 >}}
In the second session, can you force A to use K_0, since you compromised it? 
{{< /tip >}}

* {{< tip "Tip 3" tip3 >}}
Note that once you've compromised K_0, it's quite easy to get s_1, however we want to access the current session secret. 
{{< /tip >}}

* {{< tip Solution sol1 >}}
<span class='cmd'>
new_session();
transmit(0);
intercept(1);
transmit(1);
new_session();
compromise(K_0);
inject(S->A:{K_0, B, {K_0}KBS}KAS);
transmit(4);
intercept(5);
decrypt({s_2}K_0, K_0);
</span>
{{< /tip >}}


2. Can you find a sequence of commands such that B knows fake? 



* {{< tip Tip tip4 >}}
The previous attack should still work, although you might need to adjust it to the current protocol. 
{{< /tip >}}

* {{< tip Solution sol2 >}}
<span class='cmd'>
inject(E->S:E, B); transmit(0); transmit(1); decrypt({K_0, B, {K_0}KBS}KES, KES); encrypt(fake, K_0); inject(A->B:{K_0}KBS, {fake}K_0); transmit(2);
</span>
{{< /tip >}}

* {{< tip Tip tip5 >}}
As another possibility, if you have realised the attack above, you should have all the elements to send the message that B is expecting. 
{{< /tip >}}


{{< protocolgame_svg >}}
