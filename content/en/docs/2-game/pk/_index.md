 ---
title: "Public Key NS"
date: 2020-02-16
weight: 80
---
{{< pg_start >}}

Another version of the Needham-Schroeder exists, which uses public key instead of symmetric key, and which does not use a server. In order to use this version, we need to be explicit about the agents used to start the session, e.g., `new_session(A,B)`. If you just use `new_session()`, the game will try to create a session between `A` and `S`, but the latter does not exist. 

<b>Knowledge:</b> A:[s, KB, KE, A, B, NA], B:[A, B, E, KA, KB, KE, NB],
  E:[A, B, E, KA, KB, KE]<br>
<b>Steps:</b>
  <ol>
    <li> A -> B: {#NA, A}KB
    <li> B -> A: {#NB, #NA}KA
    <li> A -> B: {#NB}KB
</ol>

Can you figure out a way for B to believe they are speaking with A?


	  
{{< protocolgame >}}

<script>	  
class Sender extends Agent {

		  constructor(id, facts, network) {
			  super(id, facts, network);
			  this.session = 0;
			  this.state = 0;
			  this.currentKey = null;
			  this.default_receiver = "B"
		  }

		  // Sender has the following states
		  // init: initial state, ready to send message 1
		  // (in case of multiple sessions, message 3 from previous session
		  // has been sent)
		  // waiting_2: message 1 sent, waiting for message 2.

		  process(msg, step) {
			  switch (this.state) {
				  case 0:
					  this.init(msg, step);
					  this.state = 1;
					  break;
				  case 1:
					  this.sendReply(msg, step);
					  this.state = 0;
					  break;
				  default:
					  throw 'Unknown state for agent ' + this.id;
			  }
		  }

		  getSessionSecret(step) {
			  var secret = 's_' + this.session;
			  this.learns(secret, step)
			  return secret;
		  }

		  getNonce(step) {
			  var nonce = 'NA_' + this.session;
			  this.learns(nonce, step);
			  return nonce;
		  }

		  sendReply(msg, step) {
			  let facts = msg.content.getFacts();
			  if (facts.length != 1) {
				  console.log('Expecting exactly one argument, received: ' + facts.length);
				  return;
			  }

			  var NA_NB_KA = facts[0];
			  this.learns(NA_NB_KA, step);

			  var facts2 = this.decrypt(NA_NB_KA, 'sK' + this.id, step).getFacts();
			  if (facts2.length != 2) {
				  console.log('Expecting exactly two inner messages, received: ' + facts.length);
				  return;
			  }

			  var NA = facts2[0].trim()
			  var NB = facts2[1].trim()

			  if (NA != this.getNonce()) {
				  throw 'Wrong nonce received: ' + NA + ' (expecting ' + this.getNonce() + ')';
			  }
			  let B = msg.source
			  if (B != this.default_receiver) { 
				  throw 'Session started with ' + this.default_receiver + ' but received a message from : ' + B;
			  }
			     

			  this.sendMessage({source:this.id, destination:B, content: this.encrypt(NB, 'pK' + B, step)})

		  }



		  init(init_receiver, step) {
			  this.session++;
			  this.getSessionSecret(step);
			  this.network.pastKeys.push(this.currentKey);
			  let init_msg = this.encryptList([this.getNonce(), this.id], 'pK' + init_receiver, step);
			  this.default_receiver = init_receiver;
			  var msg = {source: this.id, destination: init_receiver, content: init_msg};
			  this.sendMessage(msg, step);
			  this.state = 1;
		  }
	  }

	  class Receiver extends Agent {
	      constructor(id, facts, network) {
			  super(id, facts, network);
			  this.state = 0;
			  this.session = 0;
		  }

		  getNonce (step) {
			  var nonce = 'N'+this.id+'_' + this.session;
			  this.learns(nonce, step);
			  return nonce;
		  }


	      process(msg, step) {
			  switch (this.state) {
				  case 0:
					  this.sendReply(msg, step);
					  break;
				  case 1:
					  this.check(msg, stp);
					  break;
				  default:
					  throw 'Unknown state for agent ' + this.id;

			  }
		  }

		  sendReply(msg, step) {
			  var facts = msg.content.getFacts();
			  if (facts.length != 1) {
				  console.log('Expecting exactly one argument, received: ' + facts.length);
				  return;
			  }

			  var NAA_KB = facts[0];
			  this.learns(NAA_KB)
			  var facts2 = this.decrypt(NAA_KB, 'sK' + this.id, step).getFacts();
			  console.log(facts2)
			  if (facts2.length != 2) {
				  console.log('Expecting exactly two inner messages, received: ' + facts.length);
				  return;
			  }

			  let NA = facts2[0];
			  let A = facts2[1];

			  let NB = this.getNonce();

			  let msg_content = this.encryptList([NA, NB], 'pK' + A, step);
			  this.sendMessage({source: this.id, destination: A, content: msg_content}, step);
		  }

		  check(msg, step) {

			  var facts = msg.content.getFacts();
			  if (facts.length != 1) {
				  console.log('Expecting exactly one argument, received: ' + facts.length);
				  return;
			  }

			  var NB_KB = facts[0];
			  this.learns(NB_KB, step);

			  var facts2 = this.decrypt(NB_KB, 'sK' + this.id, step).getFacts();
			  if (facts2.length != 1) {
				  console.log('Expecting exactly one inner messages, received: ' + facts.length);
				  return;
			  }

			  var NB = facts2[0].trim()

			  if (NB != this.getNonce()) {
				  throw 'Wrong nonce received: ' + NA + ' (expecting ' + this.getNonce() + ')';
			  }


		  }

	      decryptMessage(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 2) { 
		      throw (this.id + ': Expecting exactly two arguments, received: ' + facts.length);
		      return;
		  }

		  var KA_KBS = facts[0];
		  var S_K = facts[1];
		  this.learns(KA_KBS);
		  this.learns(S_K);

		  var facts2 = this.decrypt(KA_KBS, 'KBS', step).getFacts();
		  if (facts2.length != 2) { 
		      console.log('Expecting exactly three inner messages, received: ' + facts.length);
		      return;
		  }

		  var K = facts2[0];
		  var A = facts2[1];

		  if (A != 'A') {
		      throw 'Wrong recipient received: ' + A + ' (expecting A)'; 
		  }

		  
		  var S = this.decrypt(S_K, K, step);
		  
		  this.step = 0; 
	      }

	  }
	  
	  class Attacker extends Agent {
	      process (msg) {
		  var facts = msg.content.getFacts();
		  for (var i = 0; i < facts.length; i++)
		      this.learns(facts[i]);
	      }

	      
	      compromise(key) {
		  if (this.network.pastKeys.includes(key)) {
		      this.learns(key);
		  }
		  else
		      throw key + ' is not a past key, cannot be compromised';
	      }

	  }
	  
	  var s1 = new NetStack('message_list1');	  
	  var a1 = new Sender('A', ['s', 'A', 'B', 'E', 'pKA', 'pKB', 'pKE', 'sKA', 'NA'], s1);
	  var b1 = new Receiver('B', ['A', 'B', 'E', 'pKA', 'pKB',  'pkE', 'sKB'], s1);
	  // var v1 = new Server('S', ['KES', 'KAS', 'KBS', 'A', 'B', 'E'], s1);
	  var e1 = new Attacker('E', ['E', 'A', 'B', 'NE', 'fake', 'pKA', 'pKB', 'pKE', 'sKE' ], s1);
	  s1.registerAgents([a1, b1, e1]);
	  s1.registerAttacker(e1);
	  var c1 = new Command(s1, e1, [a1, b1, e1]);
	  
	  a1.createDiv('agents_box1', 'agent');      
	  b1.createDiv('agents_box1', 'agent');
	  e1.createDiv('agents_box1', 'agent');

	  setCurrentStep(0);
</script>

{{< protocolgame_svg >}}
