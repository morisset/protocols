 ---
title: "Fresh key"
date: 2020-02-16
weight: 40
---
{{< pg_start >}}


In the previous protocol, the problem was that the server S would simply send KBS to anyone who asks. An important concept in security protocols is to use **fresh keys**, i.e., keys that have been generated for a specific session.
Every time the server is asked for a key, it should then a different one.

Of course, this creates a problem: if A asks for a key to S, and S creates a new key, how can B get that key? This leads to another important concept in security protocol: an agent can relay an encrypted message, even if it cannot decrypt it. 

This principle is used for the protocol below: when asked for a key to communicate with B, S creates two different pieces of information: K_0 (the fresh key), and K_0 encrypted by KBS. S then encrypts everything with KAS, so that only A can decrypt it. Even though A cannot decrypt {K_0}KBS, it does not need to, since it already knows K_0. A can just send {K_0}KBS and the secret encrypted by K_0 to B, who can decrypt {K_0}KBS, and therefore the secret. 

<b>Knowledge:</b> A:[s, KAS, A, B], B:[KBS, A, B],
	  S:[A, B, E, KAS, KBS, KES, K] E:[A, B, KES, fake]<br>
	  <b>Steps:</b>
	  <ol>
	    <li> A -> S: A, B
	    <li> S -> A: {#K, {K}KBS}KAS
	    <li> A -> B: {K}KBS, {#s}K
	  </ol>	
	  
{{< protocolgame >}}

<script>	  
	  class Sender extends Agent {
	      
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.session = 0;
		  this.state = 0;
	      }
	      
	      // Sender has the following states
	      // init: initial state, ready to send message 1
	      // (in case of multiple sessions, message 3 from previous session
	      // has been sent)
	      // waiting_2: message 1 sent, waiting for message 2. 
	      
	      process (msg, step) {
		  switch (this.state) {
		  case 0:
		      this.init(step); break;
		  case 1:
		      this.sendEncryptedSecret(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		  }
	      }
	      
	      getSessionSecret (step) {
		  var secret = 's_' + this.session;
		  this.learns(secret, step)
		  return secret;
	      }
	      
	      sendEncryptedSecret(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 1) { 
		      console.log('Expecting exactly one argument, received: ' + facts.length);
		      return;
		  }
		  
		  var KBS_KAS = facts[0];
		  this.learns(KBS_KAS, step);

		  var facts2 = this.decrypt(KBS_KAS, 'KAS', step).getFacts();
		  if (facts2.length != 2) { 
		      console.log('Expecting exactly two inner messages, received: ' + facts.length);
		      return;
		  }
		  var K = facts2[0];
		  var K_KBS = facts2[1];

		  var S_K = this.encrypt(this.getSessionSecret(step), K);
		  var new_msg = {source:this.id, destination:'B',
				 content:K_KBS + ', ' +S_K};
		  this.sendMessage(new_msg, step);
		  this.state = 0;
		  
	      }
	      
	      init (step) {
		  var msg = {source:this.id, destination:'S', content:'A, B'}; 
		  this.sendMessage(msg, step);
		  this.session++;
		  this.state = 1;
	      }
	  }

	  class Receiver extends Agent {
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.state = 0;
	      }

	      process(msg, step) {
		  switch (this.state) {
		  case 0:
		      this.decryptMessage(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		      
		  }


	      }

	      decryptMessage(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 2) { 
		      throw (this.id + ': Expecting exactly two arguments, received: ' + facts.length);
		      return;
		  }

		  var K_KBS = facts[0];
		  var S_K = facts[1];
		  this.learns(K_KBS);
		  this.learns(S_K);

		  var K = this.decrypt(K_KBS, 'KBS', step);
		  var S = this.decrypt(S_K, K, step);
		  this.step = 0; 
	      }

	  }

	  class Server extends Agent {
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.state = 0;
		  this.key = null;
		  this.session = 0;
	      }

	      
	      getFreshKey(step) {
		  var k = 'K_' + this.session++;
		  this.learns(k, step)
		  this.key = k;
		  return k;
	      }

	      process (msg, step) {
		  switch (this.state) {
		  case 0:
		      this.sendEncryptedKey(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		  }
	      }
	      
	      sendEncryptedKey(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 2) { 
		      console.log('Expecting exactly two arguments, received: ' + facts.length);
		      return;
		  }
		  
		  var A = facts[0];
		  var B = facts[1];
		  var K = this.getFreshKey(step);
		  this.learns(K, step);
		  
		  var KAS = 'K' + A + 'S';
		  var KBS = 'K' + B + 'S';

		  var K_KBS = this.encrypt(K, KBS, step);
		  
		  var new_msg = {source:this.id, destination:A,
				 content:this.encryptList([K, K_KBS], KAS, step)};
		  this.sendMessage(new_msg, step);
		  
	      }

	  }
	  
	  class Attacker extends Agent {
	      process (msg) {
		  var facts = msg.content.getFacts();
		  for (var i = 0; i < facts.length; i++)
		      this.learns(facts[i]);
	      }
	  }
	  
	  var s1 = new NetStack('message_list1');	  
	  var a1 = new Sender('A', ['s', 'KAS', 'A', 'B'], s1);
	  var b1 = new Receiver('B', ['A', 'B', 'KBS'], s1);
	  var v1 = new Server('S', ['KES', 'KAS', 'KBS', 'A', 'B', 'E'], s1);
	  var e1 = new Attacker('E', ['E', 'A', 'B', 'fake', 'KES'], s1);
	  s1.registerAgents([a1, b1, v1, e1]);
	  s1.registerAttacker(e1);
	  var c1 = new Command(s1, e1, [a1, b1, e1, v1]);
	  
	  a1.createDiv('agents_box1', 'agent');      
	  b1.createDiv('agents_box1', 'agent');
	  v1.createDiv('agents_box1', 'agent');
	  e1.createDiv('agents_box1', 'agent');

	  setCurrentStep(0);
	  

</script>


## Attacks


1. Can you find a sequence of commands such that E knows s_1? 
* {{< tip "Tip 1" tip1 >}}
When S receives the initial message, it does not necessarily know the actual intended recipient that A wants to communicate with. 
{{< /tip >}}

* {{< tip "Tip 2" tip2 >}}
What happens if the server S receives the initial message "A, E", coming from A? 
{{< /tip >}}

* {{< tip Solution sol1 >}}
<span class='cmd'>
new_session();
inject(A->S:A, E);
transmit(1);
transmit(2);
intercept(3);
decrypt({K_0}KES, KES);
decrypt({s_1}K_0, K_0);
</span>
{{< /tip >}}


2. Can you find a sequence of commands such that B knows fake? 

* {{< tip Tip tip4 >}}
B is expecting a fresh key encrypted with KBS. 
{{< /tip >}}


* {{< tip Tip tip5 >}}
B cannot know whether the encryption keys have been actually generated for A. 
{{< /tip >}}


* {{< tip Tip tip6 >}}
What happens if S receives "E->S:E, B" ? 
{{< /tip >}}




* {{< tip Solution sol2 >}}
<span class='cmd'>
inject(E->S:E, B);
transmit(0);
transmit(1);
decrypt({K_0, {K_0}KBS}KES, KES);
encrypt(fake, K_0);
inject(A->B:{K_0}KBS, {fake}K_0);
transmit(2);
</span>
{{< /tip >}}



{{< protocolgame_svg >}}
