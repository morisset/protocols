 ---
title: "Basic game"
date: 2020-02-16
weight: 10
---
{{< pg_start >}}

The console enables you to execute the different commands of the attackers!

We start with five simple network control commands:
<ul>
  <li><span class='cmd'>new_session()</span>: triggers the first agent in the protocol (usually A) to start a new session of the protocol; Note that for every identifier starting with the # symbol,  <span class='cmd'>new_session</span> command will create a fresh value;  
  <li><span class='cmd'>transmit(id)</span>: transmits the message with the corresponding id in the stack;  
  <li><span class='cmd'>block(id)</span>: blocks the message with the corresponding id in the stack;  
  <li><span class='cmd'>intercept(id)</span>: intercepts the message with the corresponding id in the stack; 
  <li><span class='cmd'>inject(X -> Y : msg)</span>: injects the message "msg", where "X" and "Y" are agents in the systems. 	    
</ul>

For instance, consider the simple protocol used when introducing the concept of network: 

<b>Knowledge:</b> A:[secret], B:[], E:[fake]<br>
<b>Steps:</b>
 <ol>
  <li> A -> B: #secret
</ol>
  
Try typing in the Commands console below the different commands explained above, each command separated by a ";". For instance, executing <span class='cmd'>"new_session();transmit(0);"</span> should result in B knowing secret.

{{< protocolgame >}}

<script>

  class Sender extends Agent {
  
  constructor(id, facts, network) {
  super(id, facts, network);
  this.session = 0;
  }
  
  // Sender has the following states
  // init: initial state, ready to send message 1
  // (in case of multiple sessions, message 3 from previous session
  // has been sent)
  // waiting_2: message 1 sent, waiting for message 2. 
  
  process (msg, step) {
  this.init(step)
  }
  
  get_session_secret (step) {
  var secret = 'secret_' + this.session;
  this.learns(secret, step)
  return secret;
  }
  
  init (step) {
  var msg = {source:this.id, destination:'B', content:this.get_session_secret(step)}; 
  this.sendMessage(msg, step);
  this.session++;
  }
  }

  class Receiver extends Agent {
  process(msg, step) {
  this.learns(msg.content.getFacts()[0], step);
  }
  }
  
  var s1 = new NetStack('message_list1');	  
  var a1 = new Sender('A', ['secret'], s1);
  var b1 = new Receiver('B', [], s1);
  var e1 = new Receiver('E', ['fake'], s1);
  s1.registerAgents([a1, b1, e1]);
  s1.registerAttacker(e1);
  
  var c1 = new Command(s1, e1, [a1, b1, e1]);
  a1.createDiv('agents_box1', 'agent');      
  b1.createDiv('agents_box1', 'agent');
  e1.createDiv('agents_box1', 'agent');

  setCurrentStep(0);
  
</script>

## Attacks

1. Can you find a sequence of commands such that E knows secret_0? 
* {{< tip Tip tip1 >}}
You need to intercept the message before it is transmitted. 
{{< /tip >}}

* {{< tip Solution sol1 >}}
<span class='cmd'>new_session();intercept(0) </span>
{{< /tip >}}

2. Can you find a sequence of commands such that B knows fake? 

* {{< tip Tip tip2 >}}
You need to inject the fake message. 
{{< /tip >}}

* {{< tip Solution sol2 >}}
<span class='cmd'>inject(A->B:fake);transmit(0);</span>
{{< /tip >}}

3. Can you find a sequence of commands such that E knows secret_0 and B knows fake? 

* {{< tip Tip tip3 >}}
You need to combine both steps above. 
{{< /tip >}}

* {{< tip Solution sol3 >}}
<span class='cmd'>new_session();intercept(0);inject(A->B:fake);transmit(1);</span> {{< /tip >}}


{{< protocolgame_svg >}}
